**Guide to Setup Sonarqube for .Net Framework**

**SonarQube Installation and configuration:**

**Installation:**

•	Download the latest installers from SonarQube.org
    https://www.sonarqube.org/downloads/
    •	Unzip the sonarqube-7.*.*.zip to c:\sonarqube\ location on your local computer
    •	Navigate to c:\sonarqube\conf folder….edit the wrapper.conf such that wrapper.java.command is set to your JAVAHOME location. For example, after edit, the file should look somewhat as following based on your JAVA_HOME location:
        
        *# Path to JVM executable. By default it must be available in PATH.
        # Can be an absolute path, for example:
        #wrapper.java.command=/path/to/my/jdk/bin/java
        wrapper.java.command=C:\Program Files\Java\jdk-12.0.1\bin\java*

    •	Install the Sonar as service:
    o	Navigate to `C:\sonarqube\bin\<supported OS version>\`, It should list down bunch of .bat files
      
    On the command screen, run the InstallNTService.bat
    o	Execute the StartNTService.bat
    You should see the Sonarqube running as a local service.
     
    In the browser, you can browse `http://localhost:9000`. That shall load the SonarQube dashboard.
    Security Configuration
    •	Login as default administrator user by entering admin\admin as login\pwd
    •	After login, click on user icon on top right hand side of screen -> *My Account* -> *Security*
     
    •	Generate the *token*:
    o	Enter the name of token and hit `Generate`
    Copy the Guid Token generated and store that in a separate notepad file, you might need that later


**Install the SonarScanner**

**Installation**:

    * •	If using .Net Framework, download the Sonarscanner for MSBuild from following location (make sure version>4.0). Choose the appropriate OS version
    * https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/
    * •	Unzip the installer .zip file to c:\sonar-scanner-msbuild folder
    * Configuration:
    * •	Edit SonarQube.Analysis.xml file as per following:
    * <Property Name="sonar.host.url">http://localhost:9000</Property>
    * <Property Name="sonar.login">797399a065d92bcac3e233d1912777d9dd2596d5</Property>
    * •	Make sure to update the sonar.login property with token generated earlier during the Sonarqube installation process.
    * Sonarqube Analysis
    * •	Open the command prompt (in administrative mode)
    * •	Change the current directory to location where .sln file is located
    * Then run the following commands:

**•	Begin scan**

`C:\sonar-scanner-msbuild\SonarScanner.MSBuild.exe begin /k:"TestFrm-07262019" /n:TestFrm-App /v:1.0 /d:sonar.cs.vscoveragexml.reportsPaths="%CD%\testresults\visualstudio.coveragexml" /d:sonar.verbose="true"`
Where /k: specifies the unique key given to solution, /n: name of solution to be shown on Sonar dashboard, /v: version of project

**•	Build**

`Msbuild /t:rebuild`

**•	Collect coverage**

`"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Team Tools\Dynamic Code Coverage Tools\amd64\codecoverage.exe" collect /output:VisualStudio.coverage "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\Extensions\TestPlatform\vstest.console.exe" "WebApplication1.Tests\bin\Debug\Microsoft.VisualStudio.TestPlatform.TestFramework.dll"`

> In case of multiple test dlls, please use space as separator 

**•	Analyze and generate coveragexml**

`"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Team Tools\Dynamic Code Coverage Tools\amd64\codecoverage.exe" analyze /output:%cd%\testResults\VisualStudio.coveragexml visualstudio.coverage`

**•	End the Sonar Scan**

`C:\sonar-scanner-msbuild\SonarScanner.MSBuild.exe end`

Depending on size of project, the end step may take longer. After completion of post-processing, you shall be presented with screen similar to following:
> 23:06:32.534 DEBUG: Post-jobs :
> 23:06:37.871 INFO: Analysis total time: 43:07.590 s
> 23:06:37.890 INFO: ------------------------------------------------------------------------
> 23:06:37.890 INFO: EXECUTION SUCCESS
> 23:06:37.890 INFO: ------------------------------------------------------------------------
> 23:06:37.890 INFO: Total time: 43:10.917s
> 23:06:38.036 INFO: Final Memory: 14M/54M
> 23:06:38.036 INFO: ------------------------------------------------------------------------
> Process returned exit code 0
> The SonarQube Scanner has finished
> 23:06:38.497  Post-processing succeeded.

**Sonar Dashboard Verification:**

•	Login to http://localhost:9000
•	Projects list shall display the code metrics along with coverage stats similar to following:
  

Ref: [Stackoverflow](https://stackoverflow.com/questions/57218900/c-sharp-test-coverage-not-showing-in-sonarqube-dashboard)
					---
